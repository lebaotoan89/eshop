package diamonshop.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import diamonshop.entity.Categorys;
import diamonshop.entity.MapperCategorys;
@Repository
public class CategorysDao extends BaseDao{
	
	
	public List<Categorys> getDataCategorys(){
		List<Categorys> list= new ArrayList<Categorys>();
		String sql="SELECT * FROM `categorys`";
		list=_jdbcTemplate.query(sql, new MapperCategorys());
		return list;
	}
}