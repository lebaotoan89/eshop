package diamonshop.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import diamonshop.entity.MapperMenus;
import diamonshop.entity.Menus;
@Repository
public class MenuDao extends BaseDao{
	public List<Menus> getDataMenus(){
		List<Menus> list= new ArrayList<Menus>();
		String sql="SELECT * FROM `menus`";
		list=_jdbcTemplate.query(sql, new MapperMenus());
		return list;
	}
}