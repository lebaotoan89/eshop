package diamonshop.service.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import diamonshop.dao.ProductsDao;
import diamonshop.dto.ProductsDto;
@Service
public class CategoryServiceImpl implements IcategoryService{
	@Autowired ProductsDao productsDao;
	public List<ProductsDto> getDataProductPaginate(int id,int start, int totalPage) {
		return productsDao.getDataProductsPaginate(id,start, totalPage);
		
	}
	public List<ProductsDto> getAllProductsByID(int id) {
		
		return productsDao.getAllProductsByID(id);	
	}

}
