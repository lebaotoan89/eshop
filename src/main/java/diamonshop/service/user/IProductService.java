package diamonshop.service.user;

import java.util.List;

import org.springframework.stereotype.Service;

import diamonshop.dto.ProductsDto;

@Service
public interface IProductService {
 public ProductsDto getProductByID(long id);
 public List<ProductsDto>  getProductByIDCategory(int id);
}
	