package diamonshop.service.user;

import java.util.List;

import org.springframework.stereotype.Service;

import diamonshop.dto.ProductsDto;
@Service
public interface IcategoryService {
	public List<ProductsDto> getAllProductsByID(int id);
	public List<ProductsDto> getDataProductPaginate (int id,int start, int totalPage);
}
